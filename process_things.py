from hashlib import sha256
DASHBOARD_BLOCK_COUNTER = 0


def process_rawtx(raw):
    return

def process_rawblock(raw):
    #Set up the firs t80 blocks as a header
    block_header = raw[:80]
    version = block_header[:4]
    prev_merkle_root = block_header[4:36]
    merkle_root = block_header[36:68]

    blockhash = calculate_id(block_header)

    tx_count = 0

def calculate_id(header):
    return sha256(sha256(header).digest()).digest()[::-1]


